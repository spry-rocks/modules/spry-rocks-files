import {ICloudStorage} from './ICloudStorage';
import {Bucket, Storage} from '@google-cloud/storage';
import {Buffer} from 'buffer';
import {CloudStorageConfig} from './CloudStorageConfig';

export class CloudStorage extends ICloudStorage {
  private readonly storage: Storage;

  private readonly bucket: Bucket;

  constructor({projectId, bucket}: CloudStorageConfig) {
    super();
    this.storage = new Storage({
      projectId,
    });
    this.bucket = this.storage.bucket(bucket);
  }

  addFile(mediaLink: string, mimeType: string, buffer: Buffer) {
    return new Promise<void>((resolve, reject) => {
      this.bucket
        .file(mediaLink)
        .createWriteStream({
          gzip: true,
          contentType: mimeType,
        })
        .on('error', (e) => reject(e))
        .end(buffer, () => resolve());
    });
  }

  async getFileByName(file: string) {
    return {
      stream: await this.bucket.file(file).createReadStream({validation: false}),
    };
  }
}
