export interface CloudStorageConfig {
  projectId: string;
  bucket: string;
}
