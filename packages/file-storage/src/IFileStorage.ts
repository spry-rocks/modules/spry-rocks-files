import {Readable} from 'stream';
import {Buffer} from 'buffer';

export abstract class IFileStorage {
  abstract addFile(name: string, buffer: Buffer): Promise<void>;

  abstract getFile(file: string): Promise<{stream: Readable}>;
}
