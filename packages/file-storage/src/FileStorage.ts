import {Buffer} from 'buffer';
import {IFileStorage} from './IFileStorage';
import {FileStorageConfig} from './FileStorageConfig';
import * as fs from 'fs';

export class FileStorage extends IFileStorage {
  constructor(private config: FileStorageConfig) {
    super();
    if (!fs.existsSync(config.rootDirectory))
      throw new Error('Files directory is not exists');
  }

  async addFile(file: string, buffer: Buffer) {
    const path = this.getPath(file);
    if (fs.existsSync(path)) throw new Error(`File ${file} already exists`);
    fs.writeFileSync(`${path}`, buffer);
  }

  async getFile(file: string) {
    const path = this.getPath(file);
    const buffer = fs.createReadStream(path);
    return {stream: buffer};
  }

  private getPath(file: string) {
    return `${this.config.rootDirectory}/${file}`;
  }
}
